package com.rincliu.xreader.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.TextView;

import com.rincliu.xreader.R;
import com.rincliu.xreader.app.BaseActivity;
import com.rincliu.xreader.data.RSSReader;
import com.rincliu.xreader.data.RSSReader.ReadCallback;
import com.rincliu.xreader.data.db.ReaderDBHelper;
import com.rincliu.xreader.data.model.rss.RSSData;
import com.rincliu.xreader.util.UiUtil;

public class AddSourceActivity extends BaseActivity {

    public static final String EXTRA_SOURCE_DESC = "EXTRA_SOURCE_DESC";

    private EditText etDesc, etUrl;

    private ReaderDBHelper rdbh;
    
    private TextView tvAdd;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rdbh = ReaderDBHelper.getInstance(this);
        setContentView(R.layout.activity_add_source);
        etDesc = (EditText) findViewById(R.id.et_desc);
        etDesc.setText(getIntent().getStringExtra(EXTRA_SOURCE_DESC));
        etUrl = (EditText) findViewById(R.id.et_url);
        tvAdd = (TextView) findViewById(R.id.tv_add);
        tvAdd.setOnClickListener(this);
    }
    
    @SuppressLint("DefaultLocale")
    @Override
    public void onClick(View view){
        super.onClick(view);
        switch(view.getId()){
            case R.id.tv_add:
                if (checkInput()) {
                    final String desc = etDesc.getText().toString().trim();
                    final String url = etUrl.getText().toString().trim().toLowerCase();
                    tvAdd.setEnabled(false);
                    findViewById(R.id.pb_loading).setVisibility(View.VISIBLE);
                    RSSReader.getInstance(getApplicationContext()).read(url, new ReadCallback() {
                        @Override
                        public void onSuccess(RSSData data) {
                            tvAdd.setEnabled(true);
                            findViewById(R.id.pb_loading).setVisibility(View.GONE);
                            rdbh.putSource(url, desc);
                            UiUtil.toast(AddSourceActivity.this, R.string.add_source_success, true);
                            onBackPressed();
                        }

                        @Override
                        public void onFailure() {
                            tvAdd.setEnabled(true);
                            findViewById(R.id.pb_loading).setVisibility(View.GONE);
                            UiUtil.toast(AddSourceActivity.this, R.string.got_no_data_from_this_source, true);
                        }
                    });
                }
                break;
        }
    }

    @SuppressLint("DefaultLocale")
    private boolean checkInput() {
        String desc = etDesc.getText().toString().trim();
        String url = etUrl.getText().toString().trim().toLowerCase();
        if (TextUtils.isEmpty(desc)) {
            UiUtil.toast(this, R.string.source_desc_is_empty, true);
            return false;
        }
        if (TextUtils.isEmpty(url)) {
            UiUtil.toast(this, R.string.source_url_is_empty, true);
            return false;
        }
        if (!URLUtil.isValidUrl(url) || !URLUtil.isNetworkUrl(url)) {
            UiUtil.toast(this, R.string.source_url_is_invalid, true);
            return false;
        }
        if (rdbh.hasSource(url)) {
            UiUtil.toast(this, R.string.source_exists, true);
            return false;
        }
        if (url.endsWith("\\") || url.endsWith("/")) {
            if (rdbh.hasSource(url.substring(0, url.length() - 1))) {
                UiUtil.toast(this, R.string.source_exists, true);
                return false;
            }
        } else {
            if (rdbh.hasSource(url + "\\") || rdbh.hasSource(url + "/")) {
                UiUtil.toast(this, R.string.source_exists, true);
                return false;
            }
        }
        return true;
    }
}
