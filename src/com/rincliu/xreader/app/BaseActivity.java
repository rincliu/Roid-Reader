package com.rincliu.xreader.app;

import com.rincliu.xreader.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class BaseActivity extends Activity implements View.OnClickListener{
    
    private Handler handler;
    
    public void onHandleMessage(Message msg){};
    
    public Handler getHandler(){
        return handler;
    }
    
    @SuppressLint("HandlerLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CrashHandler.getInstance().init(this);
        handler=new Handler(){
            @Override
            public void handleMessage(Message msg){
                onHandleMessage(msg);
            }
        };
    }

    @Override
    public void setTitle(CharSequence title) {
        TextView tvTitle = (TextView) findViewById(R.id.tv_title);
        if (tvTitle != null) {
            tvTitle.setText(title);
            tvTitle.setSelected(true);
        }else{
            super.setTitle(title);
        }
    }
    
    private void initView(){
        ImageView ivTitleBack=(ImageView) findViewById(R.id.iv_title_back);
        if(ivTitleBack!=null){
            ivTitleBack.setOnClickListener(this);
        }
    }
    
    @Override
    public void setContentView(int layoutResID){
        super.setContentView(layoutResID);
        initView();
    }
    
    @Override
    public void setContentView(View view){
        super.setContentView(view);
        initView();
    }
    
    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params){
        super.setContentView(view, params);
        initView();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
    }

    @Override
    public void startActivityFromChild(Activity child, Intent intent, int requestCode) {
        super.startActivityFromChild(child, intent, requestCode);
        overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.iv_title_back:
                onBackPressed();
                break;
        }
    }
}
