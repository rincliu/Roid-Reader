package com.rincliu.xreader.data.model;

import java.io.Serializable;

public class BaseData implements Serializable
{
    private static final long serialVersionUID = 164039436997691652L;
    private boolean isCache;
    public boolean isCache()
    {
        return isCache;
    }
    public void setCache(boolean isCache)
    {
        this.isCache = isCache;
    }
}
