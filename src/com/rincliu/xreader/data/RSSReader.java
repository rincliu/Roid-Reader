package com.rincliu.xreader.data;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.rincliu.xreader.data.db.ReaderDBHelper;
import com.rincliu.xreader.data.model.rss.RSSData;
import com.rincliu.xreader.data.model.rss.RSSEntity;

public class RSSReader {
    private static RSSReader reader;

    private final Context mContext;

    public static synchronized RSSReader getInstance(Context context) {
        if (reader == null) {
            reader = new RSSReader(context);
        }
        return reader;
    }

    private RSSReader(Context context) {
        mContext = context.getApplicationContext();
    }

    public interface ReadCallback {
        public void onSuccess(RSSData data);

        public void onFailure();
    }

    public void read(String url, final ReadCallback callback) {
        new AsyncTask<String, Void, RSSData>() {

            @Override
            protected RSSData doInBackground(String... params) {
                String url = params[0];
                try {
                    Document doc = new SAXReader().read(new URL(url));
                    return parseRSSXMLDoc(url, doc);
                } catch (Exception e) {
                    e.printStackTrace();
                    return parseRSSXMLDoc(url, null);
                }
            }

            @Override
            public void onPostExecute(RSSData result) {
                if (result == null || result.getItemList() == null || result.getItemList().isEmpty()) {
                    callback.onFailure();
                } else {
                    callback.onSuccess(result);
                }
            }

        }.execute(url);
    }

    private RSSData parseRSSXMLDoc(String url, Document doc) {
        RSSData data = null;
        ReaderDBHelper rdbh = ReaderDBHelper.getInstance(mContext);
        if (doc != null) {
            rdbh.putData(url, doc.asXML());
            data = new RSSData();
            data.setCache(false);
        } else {
            String xml = rdbh.getData(url);
            if (!TextUtils.isEmpty(xml)) {
                try {
                    doc = DocumentHelper.parseText(xml);
                } catch (DocumentException e) {
                    e.printStackTrace();
                }
                data = new RSSData();
                data.setCache(true);
            }
        }
        if (doc != null) {
            Element rootEle = doc.getRootElement();
            if (rootEle.getName().equals("rss")) {
                data.setItemList(parseRssContent(rootEle));
            } else if (rootEle.getName().equals("feed")) {
                data.setItemList(parseFeedContent(rootEle));
            }
        }
        return data;
    }
    
    private ArrayList<RSSEntity> parseFeedContent(Element rootEle){
        ArrayList<RSSEntity> itemList = new ArrayList<RSSEntity>();
        @SuppressWarnings("unchecked")
        ArrayList<Element> eleList = (ArrayList<Element>) rootEle.elements();
        for (int i = 0; i < eleList.size(); i++) {
            Element ele = eleList.get(i);
            if (ele.getName().equals("entry")) {
                RSSEntity item = new RSSEntity();
                item.setTitle(ele.elementText("title"));
                item.setDate(parseDateTime(ele.elementText("updated")));
                if (ele.element("summary") != null) {
                    item.setSummary(ele.elementText("summary"));
                } else {
                    item.setSummary(ele.elementText("content"));
                }
                item.setContent(ele.elementText("content"));
                item.setLink(ele.elementText("id"));
                itemList.add(item);
            }
        }
        return itemList;
    }
    
    private ArrayList<RSSEntity> parseRssContent(Element rootEle){
        ArrayList<RSSEntity> itemList = new ArrayList<RSSEntity>();
        Element channelEle = (Element) rootEle.elements().get(0);
        @SuppressWarnings("unchecked")
        ArrayList<Element> eleList = (ArrayList<Element>) channelEle.elements();
        for (int i = 0; i < eleList.size(); i++) {
            Element ele = eleList.get(i);
            if (ele.getName().equals("item")) {
                RSSEntity item = new RSSEntity();
                item.setTitle(ele.elementText("title"));
                item.setDate(parseDateTime(ele.elementText("pubDate")));
                item.setSummary(ele.elementText("description"));
                if (ele.element("encoded") != null) {
                    item.setContent(ele.elementText("encoded"));
                } else {
                    item.setContent(ele.elementText("description"));
                }
                item.setLink(ele.elementText("link"));
                itemList.add(item);
            }
        }
        return itemList;
    }
    
    @SuppressLint("SimpleDateFormat")
    private String parseDateTime(String dateTime){
        SimpleDateFormat sdfSrc = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z");
        SimpleDateFormat sdfDes = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try
        {
            return sdfDes.format(sdfSrc.parse(dateTime));
        }
        catch (ParseException e)
        {
            e.printStackTrace();
            return dateTime;
        }
    }
}
